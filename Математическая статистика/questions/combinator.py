# combine md files into one
# get all md files in the folder

import os
import re

def get_md_files():
    files = os.listdir()
    md_files = []
    for file in files:
        if file.endswith('.md'):
            md_files.append(file)
    return md_files

def get_md_text(file):
    with open(file, 'r', encoding='utf-8') as f:
        text = f.read()
    return text

def combine():
    md_files = get_md_files()
    text = ''
    for file in md_files:
        text += get_md_text(file) + '\n'
    return text

def main():
    text = combine()
    with open('combined.md', 'w', encoding='utf-8') as f:
        f.write(text)


if __name__ == '__main__':
    main()
