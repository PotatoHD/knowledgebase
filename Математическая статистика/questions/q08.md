#### 8.	Проверка статистической гипотезы. Общая постановка задачи. Решающее правило при принятии статистического решения. Статистика критерия. Метод доверительных интервалов для проверки статистической гипотезы. Уровень значимости. Ошибки принятия статистического решения, определения и расчетные выражения. Мощность критерия. Функция мощности критерия. Примеры, графическая иллюстрация.

**Алгоритм проверки статистической гипотезы:** 
1. Сформулировать $`H_0`$, $`H'`$
2. Записать выражение для статистики критерия Z и закон распределения $`F_Z(z|H_0)`$
3. Определить тип критической области
4. Рассчитать критические точки
5. Рассчитать выборочные значения статистики критерия
6. Принять статистическое решение.

**Утверждение 1.** Общая постановка задачи проверки статистической гипотезы состоит в определении её истинности.

**Определение 1.** *Решающее правило*: если $`z\in \Omega_0`$, то основная гипотеза $`H_0`$ принимается, если $`z\in\Omega'`$, то основная гипотеза отвергается. **Замечание:** также называется статистическим критерием.

**Определение 2.** *Статистика критерия* - это статистика $`Z=Z(X_1,...,X_n)`$, на основе реализации которой $`z=Z(x_1,...,x_n)`$ выдвигается статистическое решение. Реализация статистики критерия называется *выборочным значением статистики критерия*. **Замечание:** статистика критерия должна удовлетворять следующим условиям:
1. Закон распределения $`F_Z(z|H_0)`$ должен быть известен.
2. Закон распределения должен быть чувствителен к факту справедливости основной или альтернативной гипотезы, т.е. законы $`F_Z(z|H_0)`$ и $`F_Z(z|H')`$ должны существенно различаться.

**Метод доверительных интервалов.** Пусть основная гипотеза $`H_0: θ=θ_0`$, альтернативная гипотеза $`H': θ\neθ_0`$. Если для неизвестного параметра $`θ`$ может быть построен доверительный интервал $`(θ_1; θ_2)`$, то проверка статистической гипотезы $`H_0`$ сводится к проверке попадания значения $`θ_0`$ в доверительный интервал $`(θ_1; θ_2)`$. Критерий проверки гипотез: если $`θ_0 ∈ (θ_1; θ_2)`$, то основная гипотеза $`H_0`$ должна приниматься, в противном случае – отклоняться. Если альтернативная гипотеза $`H’`$ имеет вид $`H': θ<θ_0`$ или $`H': θ>θ_0`$, то строится соответствующий односторонний доверительный интервал $`(–∞; θ_2)`$ или $`(θ_1; +∞)`$.  
При проверке статистической гипотезы о равенстве математических ожиданий строится доверительный интервал для разности m1 – m2. Если интервал накрывает 0, то основная гипотеза принимается, в противном случае – отклоняется.  
При проверке статистической гипотезы о равенстве дисперсий  строится доверительный интервал для отношения $`\sigma_1^2/\sigma^2_2`$. Если интервал накрывает 1, то основная гипотеза принимается, в противном случае – отклоняется.

**Определение 3.** *Ошибкой 1-го рода* при принятии статистического решения называется событие, состоящее в том, что основная гипотеза $`H_0`$ ***отвергается***, в то время, как ***она верна***.

**Определение 4.** *Ошибкой 2-го рода* при принятии статистического решения называется событие, состоящее в том, что основная гипотеза $`H_0`$ ***принимается***, в то время, как она ***не верна***, т.е. верна альтернативная гипотеза $`H'`$.

**Определение 5.** *Уровнем значимости* $`\alpha`$ при проверке статистической гипотезы называется *вероятность ошибки первого рода*: $`\alpha=P(Z\in\Omega'|H_0)`$.

**Определение 6.** *Вероятность ошибки второго рода* рассчитывается так: $`\beta=P(Z\in\Omega_0|H')`$.

<img src="../images/q7_1.jpg" width=450>

**Определение 7.** *Мощностью критерия* называется вероятность принятия ***H'*** при условии, что она верна. Может быть рассчитана в случае, когда альтернативная гипотеза является простой, как $`\mu=1-\beta`$.

**Определение 8.** Если альтернативная гипотеза является сложной, т.е. не определеяет однозначно функцию распределения $`F_X(x)`$, а следовательно, и функцию распределения статистики критерия $`F_Z(z|H')`$, а определяет её с точностью до значения некоторого параметра $`\theta`$, то вводят *функцию мощности критерия*: $`\mu(\theta)=1-\beta(\theta)`$, где $`\beta(\theta)`$ - вероятность ошибки 2-го рода при условии, что неизвестный параметр принял значение $`\theta`$.

**Пример 1.** $`H_0`$: самолет свой, $`H'`$: самолет чужой. Ошибки (по номеру рода ошибки):
1. Сбили свой;
2. Пропустили чужой.

**Пример 2.** $`Z|H_0\sim f_Z(Z|H_0)`$. Если $`z_b\in \Omega_0`$, то $`H_0`$ принимается, если $`z_b\in \Omega'`$, то отклоняется.

<img src="../images/q8_1.PNG" width=450> 