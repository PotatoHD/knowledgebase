## 4. Определение и классификация грамматик. Отношения =>, =>+,=>∗. Язык, порождаемый грамматикой. Примеры

* Упорядоченная 4-ка $`G=\langle V_N, V_T, S, R\rangle`$, где $`V_T`$ - алфавит терминальных символов, $`V_N`$ - алфавит нетерминальных символов ($`V_T\cap V_N=\oslash`$): S - начальный символ, $`S\in V_N`$, R - конечное мн-во правил или продукций вида $`\varphi\to\psi`$, где $`\varphi\in V_T^*V_N(V_T \cup V_N)^*`$, $`\psi \in (V_T \cup V_N)^*`$
* Цепочка $`\omega_{1}`$ непосредственно выводима из цепочки  $`\omega_{0}(\omega_{0}\Rightarrow\omega_{1})`$, если $`\exists`$ такие $`\xi_1, \xi_2, \varphi, \psi`$, что $`\omega_0=\xi_1\varphi\xi_2, \omega_1=\xi_1\psi\xi_2`$ и $`\exists`$ правило $`\varphi\to\psi`$, где $`\varphi\in(V_T\cup V_N)^*V_N(V_T\cup V_N)^*,\space \psi\in(V_T\cup V_N)^*`$ - различные цепочки
* Цепочка $`\omega_{n}`$ непосредственно выводима из цепочки $`\omega_{0}`$ за 1 или несколько  шагов $` (\omega_{0}\Rightarrow^{+}\omega_{n})`$, если $`\exists`$ последовательность цепочек $`\omega_{0},\omega_{1},\ldots,\omega_{n}\space (n\gt 0):\omega_{i}\Rightarrow\omega_{i+1}`$
* Если $`\omega_{0}\Rightarrow^{+}\omega_{n}`$ или $`\omega_{0}=\omega_{n}`$, то пишут $`\omega_{0}\Rightarrow^{*}\omega_{n}`$  

Классификация грамматик:  
* Грамматика класса 0:  
  Правила вида $`\varphi\to\psi`$, где $`\varphi\in V_T^*V_N(V_T \cup V_N)^*`$, $`\psi \in (V_T \cup V_N)^*`$
* Контекстно-зависимые(КЗ) грамматики:  
  Правила вида $`\varphi\to\psi`$, где $`\varphi\in V_T^*V_N(V_T \cup V_N)^*`$, $`\psi \in (V_T \cup V_N)^*`$  
  и $`|\varphi|\le|\psi|`$
* Контекстно-свободные(КС) грамматики:  
  Правила вида $`A\to\psi`$, где $`A\in V_N`$, $`\psi \in (V_T \cup V_N)^*`$
* A-грамматики:  
  Правила вида $`A\to a`$, $`A\to aB`$, $`A\to \lambda`$, где $`A,B\in V_N`$; $`a\in V_T`$  
* Контекстная грамматика(грамматика класса 1):  
  Правила вида $`\alpha A\beta\to\alpha\omega\beta`$, где $`\alpha ,\beta\in(V_T\cup V_N)^*, A\in V_N, \omega\in(V_T\cup V_N)^*`$  

![](https://gitlab.com/mephi-b16-504/sem5-exams/raw/master/matlinquistica/images/q4.png)  

Язык L(G), порождаемый грамматикой G - множество цепочек в основном алфавите грамматики, выводимых из начального символа

Грамматики считаются эквиваентными если эквивалентны языки порождаемые ими:  
$`G_i\sim G_j\Leftrightarrow L(G_i)\sim L(G_j)`$